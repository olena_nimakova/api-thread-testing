import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime, checkResponseSchema} from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));


describe(`Users`, () => {
    let userId: number; 
    let userEmail: string = 'testovatest52@gmail.com';
    let userPasword: string = '123456';
    let accessToken: string;
    let userName: string;
    let avatar: string;
        
    it(`user registration`, async () => {
        let userData: object = {
            avatar: 'url avatar first',
            email: userEmail,
            userName: 'test',
            password: userPasword
        };
        let response = await users.register(userData);

        checkStatusCode(response, 201);
        checkResponseTime(response, 1000);
        checkResponseSchema(response, schemas.schema_register);     
        });

        it(`return all users objects and they do not contain "null" in fields`, async () => {
            let response = await users.getAllUsers();

            checkStatusCode(response, 200);
            checkResponseTime(response, 1000);
            expect(response.body).to.be.jsonSchema(schemas.schema_allUsers);                                     
        });
        
        it(`user with valid credentials login in system`, async () => {
            let response = await auth.login(userEmail, userPasword);

            checkStatusCode(response, 200);
            checkResponseTime(response, 1000);
            checkResponseSchema(response, schemas.schema_login);
            
            accessToken = response.body.token.accessToken.token;
            userName = response.body.user.userName;
            avatar = response.body.user.avatar;
            userId = response.body.user.id;        
        });

        it(`get user by token`, async () => {
            let response = await users.getUserByToken(accessToken);

            checkStatusCode(response, 200);
            checkResponseTime(response, 1000);
            checkResponseSchema(response, schemas.schema_userBytoken);
                    
            expect(response.body.id).to.be.equal(userId);
            expect(response.body.avatar).to.be.equal(avatar);
            expect(response.body.userName).to.be.equal(userName);
            expect(response.body.email).to.be.equal(userEmail);
        });

        it(`update user personal information`, async () => {
            let userData: object = {
                id: userId,
                avatar: 'url updated avatar',
                email: userEmail,
                userName: 'new updated username',
            };
    
            let response = await users.updateUser(userData, accessToken);

            checkStatusCode(response, 204);
            checkResponseTime(response, 1000);

            avatar = 'url updated avatar';
            userName = 'new updated username';
        });   

        it(`get user by token`, async () => {
            let response = await users.getUserByToken(accessToken);

            checkStatusCode(response, 200);
            checkResponseTime(response, 1000);
            checkResponseSchema(response, schemas.schema_userBytoken);
                   
            expect(response.body.id).to.be.equal(userId);
            expect(response.body.avatar).to.be.equal(avatar);
            expect(response.body.userName).to.be.equal(userName);
            expect(response.body.email).to.be.equal(userEmail);
        });

        it(`get user details of a user with valid id`, async () => {
            let response = await users.getUserById(userId);                     
             
            checkStatusCode(response, 200);
            checkResponseTime(response, 1000);
            checkResponseSchema(response, schemas.schema_userByid);
            
            expect(response.body.id).to.be.equal(userId);
            expect(response.body.avatar).to.be.equal(avatar);
            expect(response.body.userName).to.be.equal(userName);
            expect(response.body.email).to.be.equal(userEmail);               
        });

        it(`delete user`, async () => {
            let response = await users.deleteUserById(userId, accessToken);                  
               
            checkStatusCode(response, 204);
            checkResponseTime(response, 1000);    
        }); 
                                 
    
});