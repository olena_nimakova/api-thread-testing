import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime} from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

describe('Use test data set for updating user personal information', () => {
    let userId: number = 1;
    let userEmail: string = 'testovatest52@gmail.com';
    let accessToken: string;
    let userPasword: string = '123456';
    
    before(`user registration`, async () => {
        let userData: object = {
        id: userId,
        email: userEmail,
        userName: 'test',
        password: userPasword
    };
        let response = await users.register(userData);
        checkStatusCode(response, 201);        
    });
    //if user has already registered in system then 'register' before block should be commented not to use it

    before(`login`, async () => {
        let response = await auth.login(userEmail, userPasword);

        checkStatusCode(response, 200);
        accessToken = response.body.token.accessToken.token;               
    });

    let updatedData = [
        { id: userId, avatar: 'first avatar', email: userEmail, userName: 'first username' },
        { id: userId, avatar: 'second avatar', email: 'newemail1@gmail.com', userName: 'first username'},
        { id: userId, avatar: 'second avatar', email: userEmail, userName: 'second username'},
        { id: userId, avatar: 'third avatar', email: userEmail, userName: 'third username'},
        { id: userId, avatar: 'third avatar', email: 'newemail2@gmail.com', userName: 'fourth username'},
        { id: userId, avatar: 'fourth avatar', email: 'newemail3@gmail.com', userName: 'fourth username' },
    ];

    updatedData.forEach((userData) => {
        it(`change user personal data on: '${userData.avatar}' + '${userData.email}' + '${userData.userName}'`, async () => {
            let response = await users.updateUser(userData, accessToken);

            checkStatusCode(response, 204);
            checkResponseTime(response, 1000);
        });
    });

    after(`delete user`, async () => {
        let response = await users.deleteUserById(userId, accessToken);                  
           
        checkStatusCode(response, 204);      
    }); 
});

