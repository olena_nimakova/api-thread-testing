import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime, checkResponseSchema} from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const posts = new PostsController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Negative checking`, () => {
    let userEmail: string = 'testovatest52@gmail.com';
    let userPasword: string = '123456';
    let userId: number = 1;
    let accessToken: string;
        
    it(`unable to register without username`, async () => {
        let userData: object = {
            id: userId,
            avatar: 'avatar url',
            email: userEmail,
            password: userPasword
        };
        let response = await users.register(userData);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);
        checkResponseSchema(response, schemas.schema_register_without_username);  
     });   
     
     it(`unable to register with not valid password`, async () => {
        let userData: object = {
            id: userId,
            avatar: 'avatar url',
            email: userEmail,
            userName: 'test',
            password: '2'
        };
        let response = await users.register(userData);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);
        checkResponseSchema(response, schemas.schema_register_with_bad_password);         
     }); 

     it(`not able posting a post when not authorized in system`, async () => {
        let userData: object = {
            authorId: userId,
            previewImage: "string",
            body: "string",
        };

        let response = await posts.createPost(userData, accessToken);
        checkStatusCode(response, 401);
        checkResponseTime(response, 1000);
    });  
     
});

