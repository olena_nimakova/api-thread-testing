import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime, checkResponseSchema} from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const posts = new PostsController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Posts", () => {
    let userId: number; 
    let userEmail: string = 'testovatest52@gmail.com';
    let userPasword: string = '123456';
    let accessToken: string;
    let postId: number;

    before(`user registration`, async () => {
        let userData: object = {
        email: userEmail,
        userName: 'test',
        password: userPasword
    };
        let response = await users.register(userData);
        checkStatusCode(response, 201);        
    });

    before(`login`, async () => {
        let response = await auth.login(userEmail, userPasword);

        checkStatusCode(response, 200);
        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;
        
    });

    it(`posting user post`, async () => {
        let userData: object = {
            authorId: userId,
            previewImage: "url image",
            body: "test post"  ,
        };

        let response = await posts.createPost(userData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkResponseSchema(response, schemas.schema_postUserspost);
        postId = response.body.id;
    });
    
    it(`should return 200 status code and all posts when getting the post collection`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkResponseSchema(response, schemas.schema_getAllposts);              
    });

    it(`like to user own post`, async () => {
        let userData: object = {
            entityId: postId,
            isLike: true,
            userId: userId,
        };

        let response = await posts.likePost(userData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);      
      });   

    it(`should return 200 status code and all posts when getting the post collection`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkResponseSchema(response, schemas.schema_getAllposts);        
    });

    it(`cancel like to user post`, async () => {
        let userData: object = {
            entityId: postId,
            isLike: false,
            userId: userId,
        };

        let response = await posts.likePost(userData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });   

    it(`posting another post`, async () => {
        let userData: object = {
            authorId: userId,
            previewImage: "url image",
            body: "second test post",
        };

        let response = await posts.createPost(userData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkResponseSchema(response, schemas.schema_postUserspost);               
    });

    after(`delete user`, async () => {
        let response = await users.deleteUserById(userId, accessToken);                  
           
        checkStatusCode(response, 204);      
    }); 

});
